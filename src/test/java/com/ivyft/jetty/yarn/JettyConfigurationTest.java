package com.ivyft.jetty.yarn;

import com.google.common.collect.Sets;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.io.IOUtils;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.Path;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Set;

/**
 * <pre>
 *
 * Created by zhenqin.
 * User: zhenqin
 * Date: 17/6/22
 * Time: 14:01
 * Verdor: NowledgeData
 * To change this template use File | Settings | File Templates.
 *
 * </pre>
 *
 * @author zhenqin
 */
public class JettyConfigurationTest {

    JettyConfiguration conf = new JettyConfiguration("jetty.yarn.properties");

    @Test
    public void testSave() throws Exception {
        JettyConfiguration configuration = new JettyConfiguration();
        configuration.copy(this.conf);
        Set<String> envProp = Sets.newHashSet("hello", "java");

        for (String s : envProp) {
            configuration.setProperty("jetty.yarn.instance.jvm.opts",
                    configuration.getProperty("jetty.yarn.instance.jvm.opts", "") + " " + s);

            configuration.setProperty("jetty.yarn.app.master.opts",
                    configuration.getProperty("jetty.yarn.app.master.opts", "") + " " + s);
        }


        // 没有 copy jetty.yarn.properties 这个文件，这里需要把动态参数加上
        File cFile = new File("conf", "jetty.yarn.properties");
        FileOutputStream out = new FileOutputStream(cFile, false);
        try {
            configuration.save(out);
        } catch (ConfigurationException e) {
            throw new IOException(e);
        } finally {
            IOUtils.closeQuietly(out);
        }

    }
}
