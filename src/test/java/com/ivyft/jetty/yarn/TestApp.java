package com.ivyft.jetty.yarn;

import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.yarn.api.ApplicationConstants;
import org.apache.hadoop.yarn.conf.YarnConfiguration;
import org.apache.hadoop.yarn.util.Apps;
import org.junit.Test;

import java.io.File;

/**
 * <pre>
 *
 * Created by IntelliJ IDEA.
 * User: zhenqin
 * Date: 15/12/15
 * Time: 12:03
 * To change this template use File | Settings | File Templates.
 *
 * </pre>
 *
 * @author zhenqin
 */
public class TestApp {


    @Test
    public void testGetFileName() throws Exception {
        Path p = new Path(".zip");

        int i = p.getName().lastIndexOf(".");
        String name = (i > 0 ? p.getName().substring(0, i) : p.getName());
        System.out.println(name);
    }

    @Test
    public void testHadoopStringInterner() throws Exception {
        System.out.println(org.apache.hadoop.util.StringInterner.weakIntern(" a,b,c,d "));

        YarnConfiguration configuration = new YarnConfiguration();
        String yarnClasspath = StringUtils.trimToNull(configuration.get("yarn.application.classpath", ""));
        if(StringUtils.isNotBlank(yarnClasspath)) {
            String[] strs = yarnClasspath.split(",", 0);
            for (String str : strs) {
                System.out.println(StringUtils.trim(str));
            }
        }
        System.out.println("=========");
    }
}
