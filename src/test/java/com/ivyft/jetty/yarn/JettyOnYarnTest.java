package com.ivyft.jetty.yarn;

import com.ivyft.jetty.yarn.protocol.JettyYarnClient;
import com.ivyft.jetty.yarn.protocol.NodeContainer;
import org.junit.Test;

import java.util.List;

/**
 * <pre>
 *
 * Created by IntelliJ IDEA.
 * User: zhenqin
 * Date: 15/12/1
 * Time: 13:16
 * To change this template use File | Settings | File Templates.
 *
 * </pre>
 *
 * @author zhenqin
 */
public class JettyOnYarnTest {


    String appId = "application_1450230089781_0011";



    public JettyOnYarnTest() {
    }



    @Test
    public void testStartJetty() throws Exception {
        JettyYarnClient client = JettyOnYarn.attachToApp(appId,
                new JettyConfiguration("jetty.yarn.properties")).getClient();
        //client.startMaster(512, 1, null);
        client.close();
    }



    @Test
    public void testListContainer() throws Exception {
        JettyYarnClient client = JettyOnYarn.attachToApp(appId,
                new JettyConfiguration("jetty.yarn.properties")).getClient();
        List<NodeContainer> nodeContainers = client.listInstance();
        for (NodeContainer node : nodeContainers) {
            System.out.println(node);
        }
        client.close();
    }


    @Test
    public void testShutdown() throws Exception {
        JettyYarnClient client = JettyOnYarn.attachToApp(appId,
                new JettyConfiguration("jetty.yarn.properties")).getClient();
        client.shutdown();
        client.close();
    }
}
