

package com.ivyft.jetty.yarn;

import com.ivyft.jetty.yarn.protocol.NodeContainer;
import com.ivyft.jetty.yarn.util.JettyVersion;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DataOutputBuffer;
import org.apache.hadoop.security.Credentials;
import org.apache.hadoop.security.UserGroupInformation;
import org.apache.hadoop.yarn.api.ApplicationConstants;
import org.apache.hadoop.yarn.api.records.*;
import org.apache.hadoop.yarn.client.api.async.NMClientAsync;
import org.apache.hadoop.yarn.util.Apps;
import org.apache.hadoop.yarn.util.ConverterUtils;
import org.apache.hadoop.yarn.util.Records;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.ReentrantLock;


/**
 * <pre>
 *
 * Created by IntelliJ IDEA.
 * User: zhenqin
 * Date: 15/11/28
 * Time: 19:51
 * To change this template use File | Settings | File Templates.
 *
 * </pre>
 *
 * @author zhenqin
 */
public class JettyAMRMClient implements NMClientAsync.CallbackHandler {


    /**
     * LOGGER
     */
    private static final Logger LOG = LoggerFactory.getLogger(JettyAMRMClient.class);


    /**
     * 已获得的 Container
     */
    private final Map<ContainerId, NodeContainer> CONTAINERID_NODE_MAP = new ConcurrentHashMap<ContainerId, NodeContainer>(5);


    /**
     * 正在运行的 Container
     */
    private final Map<ContainerId, NodeContainer> RUNNING_CONTAINERID_NODE_MAP = new ConcurrentHashMap<ContainerId, NodeContainer>(5);


    /**
     * 已经完成的 Container
     */
    private final Map<ContainerId, NodeContainer> COMPILED_CONTAINERID_NODE_MAP = new ConcurrentHashMap<ContainerId, NodeContainer>(5);


    private final JettyConfiguration conf;

    private final Configuration hadoopConf;


    private final BlockingQueue<Container> CONTAINER_QUEUE = new LinkedBlockingQueue<Container>(3);


    private Container currentContainer;

    private final ReentrantLock LOCK = new ReentrantLock();

    private ApplicationId appId;

    private NMClientAsync nmClient;

    public JettyAMRMClient(ApplicationId appId,
                           JettyConfiguration conf,
                           Configuration hadoopConf) {
        this.appId = appId;
        this.conf = conf;
        this.hadoopConf = hadoopConf;

        // start am nm client
        this.nmClient = NMClientAsync.createNMClientAsync(this);
        this.nmClient.init(hadoopConf);
        this.nmClient.start();


    }

    public synchronized void addAllocatedContainers(List<Container> containers) {
        if (currentContainer != null) {
            containers.remove(currentContainer);
        }
        LOCK.lock();
        try {
            for (Container container : containers) {
                if (CONTAINER_QUEUE.contains(container)) {
                    continue;
                }

                this.CONTAINER_QUEUE.put(container);
            }
        } catch (InterruptedException e) {
            LOG.info("", e);
        } finally {
            LOCK.unlock();
        }
    }



    public int containerSize() {
        return CONTAINER_QUEUE.size();
    }




    public boolean containerIsEmpty() {
        return CONTAINER_QUEUE.isEmpty();
    }




    public void startInstance(String jettyZip, List<String> wars, String jettyXml) {
        try {
            currentContainer = CONTAINER_QUEUE.take();
            LOCK.lock();
            try {
                launchJettyNodeOnContainer(currentContainer, jettyZip, wars, jettyXml);


                ContainerId containerId = currentContainer.getId();
                NodeId nodeId = currentContainer.getNodeId();

                NodeContainer jettyAndNode = new NodeContainer();
                jettyAndNode.setContainerId(containerId.toString());
                jettyAndNode.setNodeHost(nodeId.getHost());
                jettyAndNode.setNodePort(nodeId.getPort());
                jettyAndNode.setNodeHttpAddress(currentContainer.getNodeHttpAddress());

                CONTAINERID_NODE_MAP.put(containerId, jettyAndNode);
            } finally {
                currentContainer = null;
                LOCK.unlock();
            }
        } catch (Exception e) {
            LOG.error("", e);
        }
    }


    public void stopInstance(NodeContainer jettyAndNode) throws Exception {
        stopInstance(jettyAndNode.getContainerId().toString());
    }

    public void stopInstance(String id) throws Exception {
        ContainerId containerId = ConverterUtils.toContainerId(id);

        NodeContainer andNode = RUNNING_CONTAINERID_NODE_MAP.get(containerId);
        if(andNode == null) {
            andNode = CONTAINERID_NODE_MAP.get(containerId);
        }

        NodeId nodeId = NodeId.newInstance(andNode.getNodeHost().toString(), andNode.getNodePort());

        ContainerStatus containerStatus = nmClient.getClient().getContainerStatus(containerId, nodeId);
        LOG.info(containerStatus.toString());

        if(containerStatus.getState() != ContainerState.COMPLETE) {
            nmClient.getClient().stopContainer(containerId, nodeId);
        }
    }




    public List<NodeContainer> listJettyNodes() {
        List<NodeContainer> nodes = new ArrayList<NodeContainer>(3);
        Set<Map.Entry<ContainerId, NodeContainer>> entrySet = RUNNING_CONTAINERID_NODE_MAP.entrySet();
        for (Map.Entry<ContainerId, NodeContainer> entry : entrySet) {
            nodes.add(entry.getValue());
        }
        return nodes;
    }



    /**
     * Container 运行结束调用
     * @param status
     */
    public void releaseContainer(ContainerStatus status) {
        ContainerId containerId = status.getContainerId();
        NodeContainer jettyAndNode = RUNNING_CONTAINERID_NODE_MAP.get(containerId);


        RUNNING_CONTAINERID_NODE_MAP.remove(containerId);
        if(jettyAndNode == null) {
            jettyAndNode = CONTAINERID_NODE_MAP.get(containerId);
        }
        LOG.info("release container: " + containerId + "    " + jettyAndNode);

        if (null != jettyAndNode) {
            COMPILED_CONTAINERID_NODE_MAP.put(containerId, jettyAndNode);
        }

    }


    /**
     * 返回当前有几个 Container 正在运行
     * @return
     */
    public int getRunningContainer() {
        return RUNNING_CONTAINERID_NODE_MAP.size();
    }

    @Override
    public void onContainerStarted(ContainerId containerId, Map<String, ByteBuffer> allServiceResponse) {
        LOG.info("onContainerStarted: " + containerId.toString() + "    " + allServiceResponse);

        NodeContainer andNode = CONTAINERID_NODE_MAP.get(containerId);
        if (null != andNode) {
            RUNNING_CONTAINERID_NODE_MAP.put(containerId, andNode);
        }

        LOG.info("started container: " + containerId + "    " + andNode);
    }

    @Override
    public void onContainerStatusReceived(ContainerId containerId, ContainerStatus containerStatus) {
        LOG.info("onContainerStatusReceived: " + containerId.toString() + "    " + containerStatus);

        NodeContainer andNode = CONTAINERID_NODE_MAP.get(containerId);
        LOG.info("received container: " + containerId + "    " + andNode);
    }

    @Override
    public void onContainerStopped(ContainerId containerId) {
        LOG.info("onContainerStopped: " + containerId.toString());

        NodeContainer andNode = CONTAINERID_NODE_MAP.get(containerId);
        RUNNING_CONTAINERID_NODE_MAP.remove(containerId);
        if (null != andNode) {
            COMPILED_CONTAINERID_NODE_MAP.put(containerId, andNode);
        }
    }

    @Override
    public void onStartContainerError(ContainerId containerId, Throwable t) {
        LOG.info("onStartContainerError: " + containerId.toString());
        LOG.warn("start Container error. ", t);

        NodeContainer andNode = CONTAINERID_NODE_MAP.get(containerId);
        RUNNING_CONTAINERID_NODE_MAP.remove(containerId);
        if (null != andNode) {
            COMPILED_CONTAINERID_NODE_MAP.put(containerId, andNode);
        }
    }

    @Override
    public void onGetContainerStatusError(ContainerId containerId, Throwable t) {
        LOG.info("onGetContainerStatusError: " + containerId.toString());
        LOG.warn(ExceptionUtils.getFullStackTrace(t));
    }

    @Override
    public void onStopContainerError(ContainerId containerId, Throwable t) {
        LOG.info("onStopContainerError: " + containerId.toString());
        LOG.warn(ExceptionUtils.getFullStackTrace(t));
    }


    /**
     *
     * 启动 Container
     *
     * @param container Yarn Contaier
     * @param jettyZip Jetty Zip
     * @param wars war
     * @throws IOException
     */
    public void launchJettyNodeOnContainer(Container container,
                                           String jettyZip,
                                           List<String> wars,
                                           String jettyXml)
            throws IOException {
        //Path[] paths = null;
        // create a container launch context
        ContainerLaunchContext launchContext = Records.newRecord(ContainerLaunchContext.class);
        UserGroupInformation user = UserGroupInformation.getCurrentUser();
        try {
            Credentials credentials = user.getCredentials();
            DataOutputBuffer dob = new DataOutputBuffer();
            credentials.writeTokenStorageToStream(dob);
            ByteBuffer securityTokens = ByteBuffer.wrap(dob.getData(), 0, dob.getLength());
            launchContext.setTokens(securityTokens);
        } catch (IOException e) {
            LOG.warn("Getting current user info failed when trying to launch the container"
                    + e.getMessage());
        }

        String appHome = Util.getApplicationHomeForId(appId.toString());
        FileSystem fs = FileSystem.get(this.hadoopConf);
        Path dst = new Path(fs.getHomeDirectory(), appHome + Path.SEPARATOR + "AppMaster.jar");
        if(!fs.exists(dst)) {
            String containingJar = JettyOnYarn.findContainingJar(Jetty.class);
            Path src = new Path(containingJar);
            fs.copyFromLocalFile(false, true, src, dst);
            LOG.info("copy jar from: " + src + " to: " + dst);
        }

        // CLC: local resources includes jetty, conf
        Map<String, LocalResource> localResources = new HashMap<String, LocalResource>();


        localResources.put("AppMaster.jar", Util.newYarnAppResource(fs, dst));


        JettyVersion jettyVersion = JettyVersion.readFromJar();
        LOG.info(jettyVersion.getNumber());

        Path zip;
        if (StringUtils.isNotBlank(jettyZip)) {
            //自己指定的
            zip = new Path(jettyZip);
            if (!fs.exists(zip) || !fs.isFile(zip)) {
                throw new IllegalArgumentException("jetty location not exists. " + jettyZip);
            }

        } else {
            zip = new Path("/lib/jetty/jetty-" + jettyVersion.getNumber() + ".zip");
        }

        LOG.info("jetty.home=" + zip.toString());


        String vis = conf.getProperty("jetty.zip.visibility", "PUBLIC");
        if (vis.equals("PUBLIC"))
            localResources.put("lib", Util.newYarnAppResource(fs, zip,
                    LocalResourceType.ARCHIVE, LocalResourceVisibility.PUBLIC));
        else if (vis.equals("PRIVATE"))
            localResources.put("lib", Util.newYarnAppResource(fs, zip,
                    LocalResourceType.ARCHIVE, LocalResourceVisibility.PRIVATE));
        else if (vis.equals("APPLICATION"))
            localResources.put("lib", Util.newYarnAppResource(fs, zip,
                    LocalResourceType.ARCHIVE, LocalResourceVisibility.APPLICATION));

        String containerHome = appHome + Path.SEPARATOR + container.getId().getId();

        Path confDst = Util.copyClasspathConf(fs, containerHome, this.conf);
        localResources.put("conf", Util.newYarnAppResource(fs, confDst,
                LocalResourceType.FILE,
                LocalResourceVisibility.PRIVATE));

        // CLC: env
        Map<String, String> env = new HashMap<String, String>();
        env.put("JETTY_LOG_DIR", ApplicationConstants.LOG_DIR_EXPANSION_VAR);
        env.put("APPLICATION_ID", appId.toString());

        Apps.addToEnvironment(env, ApplicationConstants.Environment.CLASSPATH.name(), "./AppMaster.jar");
        Apps.addToEnvironment(env, ApplicationConstants.Environment.CLASSPATH.name(), "./conf");
        Apps.addToEnvironment(env, ApplicationConstants.Environment.CLASSPATH.name(), "./lib/*");


        launchContext.setEnvironment(env);
        launchContext.setLocalResources(localResources);

        // CLC: command
        for (String war : wars) {
            if (StringUtils.isBlank(war)) {
                throw new IllegalStateException("can not find war home." + war);
            }
        }


        List<String> masterArgs = Util.buildNodeCommands(this.conf,
                currentContainer.getResource().getMemory(),
                wars,
                jettyXml);

        LOG.info("luanch: " + StringUtils.join(masterArgs, "  "));

        launchContext.setCommands(masterArgs);

        try {
            LOG.info("Use NMClient to launch jetty node in container. ");
            nmClient.getClient().startContainer(container, launchContext);

            String userShortName = user.getShortUserName();
            if (userShortName != null)
                LOG.info("node log: http://" + container.getNodeHttpAddress() + "/node/containerlogs/"
                        + container.getId().toString() + "/" + userShortName + "/");
        } catch (Exception e) {
            LOG.error("Caught an exception while trying to start a container", e);
            throw new IllegalArgumentException(e);
        }
    }
}
