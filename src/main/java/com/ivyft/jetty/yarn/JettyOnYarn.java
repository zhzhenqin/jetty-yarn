package com.ivyft.jetty.yarn;

import com.google.common.collect.Sets;
import com.ivyft.jetty.yarn.protocol.IdType;
import com.ivyft.jetty.yarn.protocol.JettyYarnClient;
import com.ivyft.jetty.yarn.protocol.NodeContainer;
import com.ivyft.jetty.yarn.util.JettyVersion;
import org.apache.avro.AvroRemoteException;
import org.apache.commons.cli.*;
import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DataOutputBuffer;
import org.apache.hadoop.mapreduce.security.TokenCache;
import org.apache.hadoop.security.Credentials;
import org.apache.hadoop.yarn.api.ApplicationConstants;
import org.apache.hadoop.yarn.api.ApplicationConstants.Environment;
import org.apache.hadoop.yarn.api.protocolrecords.GetNewApplicationResponse;
import org.apache.hadoop.yarn.api.records.*;
import org.apache.hadoop.yarn.client.api.YarnClient;
import org.apache.hadoop.yarn.client.api.YarnClientApplication;
import org.apache.hadoop.yarn.conf.YarnConfiguration;
import org.apache.hadoop.yarn.exceptions.YarnException;
import org.apache.hadoop.yarn.util.Apps;
import org.apache.hadoop.yarn.util.ConverterUtils;
import org.apache.hadoop.yarn.util.Records;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.ByteBuffer;
import java.util.*;

/**
 * <pre>
 *
 * Created by IntelliJ IDEA.
 * User: zhenqin
 * Date: 15/12/15
 * Time: 12:52
 * To change this template use File | Settings | File Templates.
 *
 * </pre>
 *
 * @author zhenqin
 */
public class JettyOnYarn {

    private static final Logger LOG = LoggerFactory.getLogger(JettyOnYarn.class);
    public final static String YARN_REPORT_WAIT_MILLIS = "yarn.report.wait.millis";
    public final static String MASTER_HEARTBEAT_INTERVAL_MILLIS = "master.heartbeat.interval.millis";
    public final static String JETTY_MASTER_HOST = "yarn.jetty.master.host";
    public final static String MASTER_AVRO_PORT = "yarn.appmaster.avro.port";
    public final static String DEFAULT_JETTY_NODE_NUM = "yarn.jetty.node.default.num";
    public final static String MASTER_CONTAINER_PRIORITY = "yarn.master.container.priority";

    public final static String JETTY_YARN_ARCHIVES = "jetty.yarn.archives";


    protected final static HashMap<String, Command> commands = new HashMap<String, Command>();


    protected final static Command help = new Command("help", "print out this message") {


        @Override
        public Options getOpts() {
            Options options = new Options();
            options.addOption("h", "help", false, "show this message");
            return options;
        }

        @Override
        public void process(CommandLine cl) throws Exception {
            printHelpFor(cl.getArgList());
        }
    };


    protected final static Command JETTY_ON_YARN = new Command("yarn", "jetty on yarn, start/shutdown app master") {


        /**
         * Jetty On Yarn, 默认的 App Name
         */
        private String appName = "JettyOnYarn";


        /**
         * shutdown 需要的 appid
         */
        private String appId;


        /**
         * Yarn 中的线程队列名字
         */
        private String queue = "default";


        /**
         * App Master 默认的内存大小, -Xmx
         */
        private int amMB = 256;




        /**
         * App Jetty 默认的内存大小, -Xmx
         */
        private int jettyMB = 512;



        /**
         * Jetty Zip
         */
        private String jettyZip;


        /**
         * 默认的 Jetty 实例数
         */
        private int defaultInstance = 1;


        /**
         * AppWars
         */
        private List<String> appWars = new ArrayList<String>(0);


        /**
         * archives
         */
        private List<String> archives = new ArrayList<String>(0);



        @Override
        public Options getOpts() {
            Options options = new Options();
            options.addOption("appid", "appid", true, "App Id, JettyOnYarn ApplicationMaster ID");
            options.addOption("wars", "wars", true, "App Wars, Java EE War");
            options.addOption("archives", "archives", true, "App Dependency archives, Zip");
            options.addOption("shutdown", "shutdown", false, "App Name, shutdown JettyOnYarn");
            options.addOption("n", "appname", true, "App Name, JettyOnYarn");
            options.addOption("im", "instance-memory", true, "Jetty Instance Memory, default 512M");
            options.addOption("ic", "instance-cores", true, "Jetty Instance Cores, default 1");
            options.addOption("q", "queue", true, "Hadoop Yarn Queue, default");
            options.addOption("e", "env", true, "Dynamic System Properties");
            options.addOption("m", "memory", true, "ApplicationMaster Memory, default 256M");
            options.addOption("c", "core", true, "ApplicationMaster Cores, default 1");
            options.addOption("z", "zip", true, "Jetty Zip Location, default /lib/jetty/jetty-{version}.zip");
            options.addOption("s", false, "print exception");
            return options;
        }

        @Override
        public void process(CommandLine cl) throws Exception {
            if (cl.hasOption("shutdown")) {
                this.appId = cl.getOptionValue("appid");
                if (StringUtils.isBlank(appId)) {
                    throw new IllegalArgumentException("app id must not be null.");
                }

                ApplicationId applicationId = ConverterUtils.toApplicationId(appId);
                System.out.println(applicationId);
                shutdown(new JettyConfiguration("jetty.yarn.properties"));
                return;
            }

            String wars = cl.getOptionValue("wars");
            if (StringUtils.isNotBlank(wars)) {
                String[] strings = wars.split(",");
                for (String s : strings) {
                    String war = StringUtils.trimToNull(s);
                    if(StringUtils.isNotBlank(war)) {
                        this.appWars.add(war);
                    }
                }
            } else {
                throw new IllegalArgumentException("wars must not be null.");
            }

            String archives = cl.getOptionValue("archives");
            if (StringUtils.isNotBlank(archives)) {
                String[] strings = archives.split(",");
                for (String s : strings) {
                    String zipFile = StringUtils.trimToNull(s);
                    if(StringUtils.isNotBlank(zipFile)) {
                        this.archives.add(zipFile);
                    }
                }
            }


            System.out.println("starting jetty on yarn, input Y/N");
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            String s = reader.readLine();
            if (!StringUtils.equals("Y", StringUtils.upperCase(s))) {
                return;
            }
            String appName = cl.getOptionValue("n");
            if (StringUtils.isNotBlank(appName)) {
                this.appName = appName;
            }

            String queue = cl.getOptionValue("q");
            if (StringUtils.isNotBlank(queue)) {
                this.queue = queue;
            }

            String m = cl.getOptionValue("m");
            if (StringUtils.isNotBlank(m)) {
                this.amMB = Integer.parseInt(m);
            }


            m = cl.getOptionValue("im");
            if (StringUtils.isNotBlank(m)) {
                this.jettyMB = Integer.parseInt(m);
            }


            String jettyZip = cl.getOptionValue("z");
            if (StringUtils.isNotBlank(jettyZip)) {
                this.jettyZip = jettyZip;
            }

            int appMasterCores = 1;
            int jettyInstanceCores = 1;
            if(cl.hasOption("c")) {
                appMasterCores = Integer.parseInt(cl.getOptionValue("c"));

                if(appMasterCores < 1) {
                    appMasterCores = 1;
                }
            }
            if(cl.hasOption("ic")) {
                jettyInstanceCores = Integer.parseInt(cl.getOptionValue("ic"));

                if(jettyInstanceCores < 1) {
                    jettyInstanceCores = 1;
                }
            }

            Set<String> envP = new HashSet<String>(3);
            if(cl.hasOption("e")) {
                envP = Sets.newHashSet(cl.getOptionValue("e").split(","));
            }

            JettyConfiguration conf = new JettyConfiguration("jetty.yarn.properties");
            launchApplication(this.appName, this.queue, amMB, appMasterCores, conf, this.jettyZip,
                    appWars, this.archives, envP, jettyMB, jettyInstanceCores, defaultInstance);
        }


        public void shutdown(JettyConfiguration conf) throws Exception {
            JettyYarnClient client = attachToApp(appId,
                    conf).getClient();
            client.shutdown();
            client.close();
        }

    };





    protected final static Command JETTY_ON_YARN_ADD = new Command("yarn-add", "jetty on yarn, add jetty instance") {


        private String appId;
        private int cores = 1;
        private int mb = 512;
        private String jettyZip = "";
        private String wars;
        private String jettyXml = "conf/jetty.xml";

        @Override
        public Options getOpts() {
            Options options = new Options();
            options.addOption("appid", "appid", true, "App Id, JettyOnYarn ApplicationMaster ID");
            options.addOption("wars", "wars", true, "JavaEE War Location, HDFS direction.");

            options.addOption("m", "memory", true, "Jetty Instance Memory, default 512M");
            options.addOption("c", "core", true, "Jetty Instance Cores, default 1");
            options.addOption("z", "zip", true, "Jetty Zip Location, default /lib/jetty/jetty-{version}.zip");
            options.addOption("x", "xml", true, "Jetty XML, default conf/jetty.xml");

            options.addOption("s", false, "print exception");
            return options;
        }

        @Override
        public void process(CommandLine cl) throws Exception {
            this.appId = cl.getOptionValue("appid");
            if(StringUtils.isBlank(appId)) {
                throw new IllegalArgumentException("app id must not be null.");
            }

            ApplicationId applicationId = ConverterUtils.toApplicationId(appId);
            System.out.println(applicationId);

            String m = cl.getOptionValue("m");
            if(StringUtils.isNotBlank(m)) {
                this.mb = Integer.parseInt(m);
            }

            String c = cl.getOptionValue("c");
            if(StringUtils.isNotBlank(c)) {
                this.cores = Integer.parseInt(c);
            }

            String jettyZip = cl.getOptionValue("z");
            if(StringUtils.isNotBlank(jettyZip)) {
                this.jettyZip = jettyZip;
            }

            String jettyXml = cl.getOptionValue("x");
            if(StringUtils.isNotBlank(jettyXml)) {
                this.jettyXml = jettyXml;
            }

            this.wars = cl.getOptionValue("wars");
            List<CharSequence> warList = new ArrayList<CharSequence>();
            if(StringUtils.isNotBlank(wars)) {
                warList.addAll(Arrays.<CharSequence>asList(wars.split(",")));
            }

            JettyConfiguration conf = new JettyConfiguration("jetty.yarn.properties");
            JettyYarnClient client = attachToApp(appId, conf).getClient();
            client.addInstance(this.mb,
                    this.cores,
                    this.jettyZip,
                    warList,
                    this.jettyXml
            );
            client.close();
        }
    };





    protected final static Command JETTY_ON_YARN_LIST = new Command("yarn-list", "jetty on yarn, list jetty instance") {


        private String appId;


        @Override
        public Options getOpts() {
            Options options = new Options();
            options.addOption("appid", "appid", true, "App Id, JettyOnYarn ApplicationMaster ID");
            options.addOption("s", false, "print exception");
            return options;
        }

        @Override
        public void process(CommandLine cl) throws Exception {
            String appid = cl.getOptionValue("appid");
            if (StringUtils.isNotBlank(appid)) {
                this.appId = appid;
            } else {
                throw new IllegalArgumentException("appid must not be null");
            }

            JettyConfiguration conf = new JettyConfiguration("jetty.yarn.properties");
            JettyYarnClient client = attachToApp(appId, conf).getClient();
            List<NodeContainer> nodeContainers = client.listInstance();

            LOG.info("appid: " + appId + " jetty instance size=" + nodeContainers.size());
            for (NodeContainer container : nodeContainers) {
                System.out.println(container);
            }
            client.close();
        }
    };





    protected final static Command JETTY_ON_YARN_STOP = new Command("yarn-stop", "jetty on yarn, stop jetty instance") {


        private String appId;


        private String containerId;


        private String nodeId;



        private boolean all = false;


        @Override
        public Options getOpts() {
            Options options = new Options();
            options.addOption("appid", "appid", true, "App Id, JettyOnYarn ApplicationMaster ID");
            options.addOption("c", "containerid", true, "Jetty Container Id, JettyOnYarn yarn-list");
            options.addOption("n", "nodeid", true, "Jetty Instance NodeId, JettyOnYarn yarn-stop");
            options.addOption("a", "all", false, "Stop All Jetty Instance, --all");
            options.addOption("s", false, "print exception");
            return options;
        }

        @Override
        public void process(CommandLine cl) throws Exception {
            String appid = cl.getOptionValue("appid");
            if (StringUtils.isNotBlank(appid)) {
                this.appId = appid;
            } else {
                throw new IllegalArgumentException("appid must not be null");
            }

            if(cl.hasOption("a")) {
                all = true;
            }

            if(!all) {
                if(cl.hasOption("c")) {
                    this.containerId = cl.getOptionValue("c");
                }

                if(cl.hasOption("n")) {
                    this.nodeId = cl.getOptionValue("n");
                }

                if (StringUtils.isBlank(containerId) && StringUtils.isBlank(nodeId)) {
                    throw new IllegalArgumentException("containerId or nodeId has value, not any null");
                }

                JettyConfiguration conf = new JettyConfiguration("jetty.yarn.properties");
                JettyYarnClient client = attachToApp(appId, conf).getClient();
                if(StringUtils.isNotBlank(containerId)) {
                    client.stopInstance(containerId, IdType.CONTAINER_ID);
                } else if(StringUtils.isNotBlank(nodeId)) {
                    client.stopInstance(nodeId, IdType.NODE_ID);
                }
                client.close();
            } else {
                // stop all
                JettyConfiguration conf = new JettyConfiguration("jetty.yarn.properties");
                JettyYarnClient client = attachToApp(appId, conf).getClient();
                client.stopAll();
                client.close();
            }
        }
    };



    public static void printHelpFor(Collection<String> args) {
        if (args == null || args.size() < 1) {
            args = commands.keySet();
        }
        HelpFormatter f = new HelpFormatter();
        for (String command : args) {
            Command c = commands.get(command);
            if (c != null) {
                f.printHelp(command, c.getHeaderDescription(), c.getOpts(), null);
                System.out.println();
            } else {
                System.err.println("ERROR: " + c + " is not a supported command.");
            }
        }
    }

    private static String printUsageFooter() {
        return "\nGlobal Options:" +
                "  -s\t\tshow stacktrace\n";
    }

    private static void printUsageHeader() {
        System.err.println("Usage: ");
    }


    private static String[] removeArg(String[] args, String argToRemove) {
        List<String> newArgs = new ArrayList<String>();
        for (String arg : args) {
            if (!arg.equals(argToRemove)) {
                newArgs.add(arg);
            }
        }
        return newArgs.toArray(new String[newArgs.size()]);
    }


    protected static Map<String, String> parseOptionMap(final String[] args) {
        Map<String, String> optionMap = new HashMap<String, String>();
        for (int i = 0; i < args.length; i++) {
            if (args[i].startsWith("-")) {
                String value = null;
                if (i < args.length - 1 && !args[i + 1].startsWith("-")) {
                    value = args[i + 1];
                }
                optionMap.put(args[i], value);
            }
        }
        return optionMap;
    }


    private static void printError(String errorMsg) {
        System.err.println("ERROR: " + errorMsg);
    }


    //private ApplicationClientProtocol appClientProtocol;
    private YarnClient _yarn;
    private YarnConfiguration _hadoopConf;
    private ApplicationId _appId;
    private JettyConfiguration conf;
    private JettyYarnClient _client = null;

    private JettyOnYarn(JettyConfiguration jettyConf) {
        this(null, jettyConf);
    }

    private JettyOnYarn(ApplicationId appId, JettyConfiguration jettyConf) {
        _hadoopConf = new YarnConfiguration();
        _yarn = YarnClient.createYarnClient();
        this.conf = jettyConf;
        _appId = appId;
        _yarn.init(_hadoopConf);
        _yarn.start();
    }

    public void stop() {
        if (_client != null) {
            try {
                _client.shutdown();
            } catch (AvroRemoteException e) {
                LOG.error(e.getMessage());
            }
        }
        _yarn.stop();
    }

    public ApplicationId getAppId() {
        return _appId;
    }

    public synchronized JettyYarnClient getClient() throws YarnException, IOException {
        if (_client == null) {
            String host = null;
            int port = 0;
            //wait for application to be ready
            int max_wait_for_report = conf.getInt(YARN_REPORT_WAIT_MILLIS, 60000);
            int waited = 0;
            while (waited < max_wait_for_report) {
                ApplicationReport report = _yarn.getApplicationReport(_appId);
                host = report.getHost();
                port = report.getRpcPort();
                LOG.info("application master start at " + host + ":" + port);
                if (host == null || port == 0) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                    }
                    waited += 1000;
                } else {
                    break;
                }
            }
            if (host == null || port == 0) {
                LOG.info("No host/port returned for Application Master " + _appId);
                return null;
            }

            LOG.info("application report for " + _appId + " :" + host + ":" + port);
            LOG.info("Attaching to " + host + ":" + port + " to talk to app master " + _appId);
            _client = new JettyYarnClient(host, port);
        }
        return _client;
    }


    /**
     * 启动 AppMaster
     * @param appName
     * @param queue
     * @param amMB
     * @param jettyZip
     * @throws Exception
     */
    private void launchApp(String appName,
                           String queue,
                           int amMB,
                           int masterCores,
                           String jettyZip,
                           List<String> wars,
                           List<String> archives,
                           Set<String> envProp,
                           int jettyMB,
                           int jettyCores,
                           int defaultInstance) throws Exception {
        LOG.info("JettyOnYarn:launchApp() ...");
        YarnClientApplication client_app = _yarn.createApplication();
        GetNewApplicationResponse app = client_app.getNewApplicationResponse();
        _appId = app.getApplicationId();
        LOG.info("_appId:" + _appId);

        if (amMB > app.getMaximumResourceCapability().getMemory()) {
            //TODO need some sanity checks
            amMB = app.getMaximumResourceCapability().getMemory();
        }


        ApplicationSubmissionContext appContext =
                Records.newRecord(ApplicationSubmissionContext.class);
        appContext.setApplicationId(app.getApplicationId());
        appContext.setApplicationName(appName);
        appContext.setQueue(queue);
        appContext.setApplicationType("JETTYONYARN");
        //appContext.setUnmanagedAM(true);
        
       
        // Set the priority for the application master
        Priority pri = Records.newRecord(Priority.class);
        pri.setPriority(0);
        appContext.setPriority(pri);
  

        // Set up the container launch context for the application master
        ContainerLaunchContext amContainer = Records
                .newRecord(ContainerLaunchContext.class);
        Credentials credentials = new Credentials();
        DataOutputBuffer dob = new DataOutputBuffer();
        credentials.writeTokenStorageToStream(dob);
        ByteBuffer securityTokens = ByteBuffer.wrap(dob.getData(), 0, dob.getLength());
        //security tokens for HDFS distributed cache
        amContainer.setTokens(securityTokens);


        Resource capability = Records.newRecord(Resource.class);
        capability.setMemory(amMB);
        capability.setVirtualCores(masterCores);
        appContext.setResource(capability);

        Map<String, LocalResource> localResources = new HashMap<String, LocalResource>();

        // set local resources for the application master
        // local files or archives as needed
        // In this scenario, the jar file for the application master is part of the
        // local resources
        LOG.info("Copy App Master jar from local filesystem and add to local environment");
        // Copy the application master jar to the filesystem
        // Create a local resource to point to the destination jar path
        String appMasterJar = findContainingJar(JettyAppMaster.class);
        LOG.info("appMasterJar: " + appMasterJar);

        FileSystem fs = FileSystem.get(_hadoopConf);

        Path src = new Path(appMasterJar);
        String appHome = Util.getApplicationHomeForId(_appId.toString());
        Path dst = new Path(fs.getHomeDirectory(),
                appHome + Path.SEPARATOR + "AppMaster.jar");
        fs.copyFromLocalFile(false, true, src, dst);
        LOG.info("copy jar from: " + src + " to: " + dst);
        localResources.put("AppMaster.jar", Util.newYarnAppResource(fs, dst));

        Path zip;
        if (StringUtils.isNotBlank(jettyZip)) {
            //自己指定的
            zip = new Path(jettyZip);
            if (!fs.exists(zip) || !fs.isFile(zip)) {
                throw new IllegalArgumentException("jetty zip location not exists. " + jettyZip);
            }
        } else {
            JettyVersion jettyVersion = JettyVersion.readFromJar();
            LOG.info(jettyVersion.getNumber());
            zip = new Path("/lib/jetty/jetty-" + jettyVersion.getNumber() + ".zip");
        }

        LocalResourceVisibility visibility = LocalResourceVisibility.PUBLIC;
        conf.setProperty("jetty.zip.path", zip.makeQualified(fs).toUri().getPath());
        conf.setProperty("jetty.zip.visibility", "PUBLIC");
        if (!Util.isPublic(fs, zip)) {
            visibility = LocalResourceVisibility.APPLICATION;
            conf.setProperty("jetty.zip.visibility", "APPLICATION");
        }
        localResources.put("lib", Util.newYarnAppResource(fs, zip, LocalResourceType.ARCHIVE, visibility));

        for (String archive : archives) {
            Path p = new Path(archive);

            int i = p.getName().lastIndexOf(".");
            String name = (i > 0 ? p.getName().substring(0, i) : p.getName());

            LOG.info("add archive file: {}", archive);
            localResources.put(name, Util.newYarnAppResource(fs, p, LocalResourceType.ARCHIVE, visibility));
        }

        JettyConfiguration configuration = new JettyConfiguration();
        configuration.copy(this.conf);
        configuration.setProperty(JETTY_YARN_ARCHIVES, StringUtils.join(archives, ","));

        for (String s : envProp) {
            configuration.setProperty("jetty.yarn.instance.jvm.opts",
                    configuration.getProperty("jetty.yarn.instance.jvm.opts", "") + " " + s);
            configuration.setProperty("jetty.yarn.app.master.opts",
                    configuration.getProperty("jetty.yarn.app.master.opts", "") + " " + s);
        }

        Path confDst = Util.copyClasspathConf(fs, appHome, configuration);
        // establish a symbolic link to conf directory
        localResources.put("conf", Util.newYarnAppResource(fs, confDst));

        int rs = 3;
        // Setup security tokens
        Path[] paths = new Path[rs];
        paths[0] = dst;
        paths[1] = zip;
        paths[2] = confDst;


        TokenCache.obtainTokensForNamenodes(credentials, paths, _hadoopConf);

        // Set local resource info into app master container launch context
        amContainer.setLocalResources(localResources);

        // Set the env variables to be setup in the env where the application master
        // will be run
        LOG.info("Set the environment for the application master");
        Map<String, String> env = new HashMap<String, String>();
        // add the runtime classpath needed for tests to work
        Apps.addToEnvironment(env, Environment.CLASSPATH.name(), "./AppMaster.jar");
        Apps.addToEnvironment(env, Environment.CLASSPATH.name(), "./conf");
        Apps.addToEnvironment(env, Environment.CLASSPATH.name(), "./lib/*");

        String yarnClasspath = StringUtils.trimToNull(_hadoopConf.get("yarn.application.classpath", ""));
        if(StringUtils.isNotBlank(yarnClasspath)) {
            String[] strs = yarnClasspath.split(",", 0);
            for (String str : strs) {
                Apps.addToEnvironment(env, Environment.CLASSPATH.name(), StringUtils.trim(str));
            }

        }
        env.put("appId", _appId.toString());
        env.put("JETTY_LOG_DIR", ApplicationConstants.LOG_DIR_EXPANSION_VAR);

        amContainer.setEnvironment(env);

        List<String> vargs = Util.buildAppMasterCommands(conf, amMB, wars, jettyMB, jettyCores,
                appName, queue, dst, getAppId(), defaultInstance);
        // Set java executable command
        LOG.info("Setting up app master command:" + vargs);

        amContainer.setCommands(vargs);


        appContext.setAMContainerSpec(amContainer);
        _yarn.submitApplication(appContext);
    }


    /**
     * Wait until the application is successfully launched
     *
     * @throws YarnException
     */
    public boolean waitUntilLaunched() throws YarnException, IOException {
        while (true) {
            // Get application report for the appId we are interested in
            ApplicationReport report = _yarn.getApplicationReport(_appId);

            LOG.info(report.toString());


            YarnApplicationState state = report.getYarnApplicationState();
            FinalApplicationStatus dsStatus = report.getFinalApplicationStatus();
            if (YarnApplicationState.FINISHED == state) {
                if (FinalApplicationStatus.SUCCEEDED == dsStatus) {
                    LOG.info("Application has completed successfully. Breaking monitoring loop");
                    return true;
                } else {
                    LOG.info("Application did finished unsuccessfully."
                            + " YarnState=" + state.toString() + ", DSFinalStatus=" + dsStatus.toString()
                            + ". Breaking monitoring loop");
                    return false;
                }
            } else if (YarnApplicationState.KILLED == state
                    || YarnApplicationState.FAILED == state) {
                LOG.info("Application did not finish."
                        + " YarnState=" + state.toString() + ", DSFinalStatus=" + dsStatus.toString()
                        + ". Breaking monitoring loop");
                return false;
            }

            // Check app status every 1 second.
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                LOG.debug("Thread sleep in monitoring loop interrupted");
            }

            //announce application master's host and port
            if (state == YarnApplicationState.RUNNING) {
                LOG.info(report.getApplicationId() + " luanched, status: " + state);
                return true;
            }
        }
    }


    /**
     * Find a jar that contains a class of the same name, if any.
     * It will return a jar file, even if that is not the first thing
     * on the class path that has a class with the same name.
     *
     * @param my_class the class to find.
     * @return a jar file that contains the class, or null.
     * @throws IOException on any error
     */
    public static String findContainingJar(Class<?> my_class) throws IOException {
        String appJar = System.getProperty("app.jar");
        if (StringUtils.isNotBlank(appJar)) {
            //jetty-yarn/target/jetty-yarn.jar
            return new File(appJar).toURI().toString();
        }
        ClassLoader loader = my_class.getClassLoader();
        String class_file = my_class.getName().replaceAll("\\.", "/") + ".class";
        for (Enumeration<URL> itr = loader.getResources(class_file);
             itr.hasMoreElements(); ) {
            URL url = itr.nextElement();
            if ("jar".equals(url.getProtocol())) {
                String toReturn = url.getPath();
                if (toReturn.startsWith("file:")) {
                    toReturn = toReturn.substring("file:".length());
                }
                // URLDecoder is a misnamed class, since it actually decodes
                // x-www-form-urlencoded MIME type rather than actual
                // URL encoding (which the file path has). Therefore it would
                // decode +s to ' 's which is incorrect (spaces are actually
                // either unencoded or encoded as "%20"). Replace +s first, so
                // that they are kept sacred during the decoding process.
                toReturn = toReturn.replaceAll("\\+", "%2B");
                toReturn = URLDecoder.decode(toReturn, "UTF-8");
                return toReturn.replaceAll("!.*$", "");
            }
        }
        throw new IOException("Fail to locat a JAR for class: " + my_class.getName());
    }


    /**
     * 简易启动接口
     * @param appName AppName， 可以自己指定
     * @param jettyConf Jetty On Yarn Configuration
     * @param wars 需要部署的 Web 应用
     * @param archives 附件
     * @param envProp 动态的 JVM 参数
     * @param jettyMB 容器实例化内存大小
     * @param jettyCores 容器实例化 Cores
     * @return 返回Yarn 程序
     * @throws Exception
     */
    public static JettyOnYarn launchApplication(String appName,
                                                JettyConfiguration jettyConf,
                                                String jettyZip,
                                                List<String> wars,
                                                List<String> archives,
                                                Set<String> envProp,
                                                int jettyMB,
                                                int jettyCores) throws Exception {
        return launchApplication(appName, "default", 256, 1, jettyConf, jettyZip, wars, archives, envProp, jettyMB, jettyCores, 1);
    }


    public static JettyOnYarn launchApplication(String appName,
                                                String queue,
                                                int amMB,
                                                int appMasterCores,
                                                JettyConfiguration jettyConf,
                                                String jettyZip,
                                                List<String> wars,
                                                List<String> archives,
                                                Set<String> envProp,
                                                int jettyMB,
                                                int jettyCores,
                                                int defaultInstance) throws Exception {
        JettyOnYarn jettyOnYarn = new JettyOnYarn(jettyConf);
        jettyOnYarn.launchApp(appName, queue, amMB, appMasterCores,
                jettyZip, wars, archives, envProp, jettyMB, jettyCores, defaultInstance);
        jettyOnYarn.waitUntilLaunched();
        return jettyOnYarn;
    }

    public static JettyOnYarn attachToApp(String appId, JettyConfiguration jettyConf) {
        return new JettyOnYarn(ConverterUtils.toApplicationId(appId), jettyConf);
    }


    public static void main(String[] args) throws ParseException {
        commands.put(help.getCommand(), help);
        commands.put(JETTY_ON_YARN.getCommand(), JETTY_ON_YARN);
        commands.put(JETTY_ON_YARN_ADD.getCommand(), JETTY_ON_YARN_ADD);
        commands.put(JETTY_ON_YARN_LIST.getCommand(), JETTY_ON_YARN_LIST);
        commands.put(JETTY_ON_YARN_STOP.getCommand(), JETTY_ON_YARN_STOP);


        String commandName = null;
        String[] commandArgs = null;
        if (args.length == 0) {
            commandName = "help";
            commandArgs = new String[0];
        } else {
            commandName = args[0];
            if(args.length > 1) {
                commandArgs = Arrays.copyOfRange(args, 1, args.length);
            } else {
                commandArgs = new String[0];
            }
        }

        Command command = commands.get(commandName);
        if (command == null) {
            LOG.error("ERROR: " + commandName + " is not a supported command.");
            printHelpFor(null);
            System.exit(1);
        }
        CommandLine cl = new GnuParser().parse(command.getOpts(), commandArgs);
        if (cl.hasOption("h") || cl.hasOption("help")) {
            printHelpFor(Arrays.asList(commandName));
            return;
        }

        boolean showStackTrace = cl.hasOption("s");
        try {
            command.process(cl);
        } catch (Exception e) {
            if (showStackTrace) {
                e.printStackTrace();
            } else {
                printError(e.getMessage());
            }
            HelpFormatter f = new HelpFormatter();
            f.printHelp(command.getCommand(),
                    command.getHeaderDescription(),
                    command.getOpts(),
                    showStackTrace ? null : printUsageFooter());
            System.exit(1);
        }
    }

}
