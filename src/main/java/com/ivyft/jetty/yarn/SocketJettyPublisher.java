package com.ivyft.jetty.yarn;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <pre>
 *
 * Created by IntelliJ IDEA.
 * User: zhenqin
 * Date: 15/12/16
 * Time: 16:20
 * To change this template use File | Settings | File Templates.
 *
 * </pre>
 *
 * @author zhenqin
 */
public class SocketJettyPublisher implements JettyPublisher {




    private static Logger LOG = LoggerFactory.getLogger(SocketJettyPublisher.class);



    @Override
    public void init(JettyConfiguration jettyConf) {

    }

    @Override
    public void publish(String host, int port) {
        LOG.info("jetty server published socket at: [" + host + ":" + port + "]");
    }
}
