package com.ivyft.jetty.yarn;

/**
 * <pre>
 *
 * Created by IntelliJ IDEA.
 * User: zhenqin
 * Date: 15/12/16
 * Time: 16:17
 * To change this template use File | Settings | File Templates.
 *
 * </pre>
 *
 * @author zhenqin
 */
public interface JettyPublisher {


    public void init(JettyConfiguration jettyConf);



    public void publish(String host, int port);

}
